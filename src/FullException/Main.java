package FullException;


public class Main {
public static void main(String[] args) throws FullException{
		
		try{
			Refrigerator re = new Refrigerator();
			String stuff = "Hamburger" + " Milk";
			re.takeOut(stuff);
			re.toString();
			System.out.println("This Refrigerator take out " + stuff + " => Status not empty");
		}
		
		finally {
			System.out.print("End program");
		}
	
	}
}
