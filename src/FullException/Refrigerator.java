package FullException;

import java.util.ArrayList;

public class Refrigerator {
	private int size;
	ArrayList<String> things = new ArrayList<String>();
	
	public void put(String stuff) throws FullException{
		if (things != null){
			things.add(stuff);
		}
		else{
			throw new FullException();
		}
	}
	
	public String takeOut(String stuff){
		if(stuff != null){
			things.remove(stuff);
			return stuff;
		}
		else{
			return stuff;
		}
	}
	
	public String toString(){
		/* for (String i : things) {
			System.out.println(i);
		} */
		String lst = things.toString();
			return "The refrigerator have " + lst;
	}
}
