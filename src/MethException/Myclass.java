package MethException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Myclass {
	public void methX() throws DataException{
		throw new DataException("You do not put any Data.");
	}
	
	public void methY() {
		throw new FormatException("Format of Data Error.");
	}
	
	public static void main(String[] args) {
		try{
			Myclass c = new Myclass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;
		} catch(DataException e){
			System.out.print("D");
		} catch(FormatException e){
			System.out.print("E");
		} finally {
			System.out.print("F");
		}
		System.out.print("G");
	}
}
