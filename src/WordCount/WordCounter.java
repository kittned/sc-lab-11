package WordCount;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WordCounter {
	public HashMap<String,Integer> wordCount;
	private String word;
	private String message;
	
	public WordCounter(String message){
		HashMap<String,Integer> Countmap = new HashMap<String,Integer>() ;
		this.message = message;
	}
	
	public void count(){
		String[] lstword = message.split("\\s");
		HashMap<String,Integer> p = new HashMap<String,Integer>() ;
		for(String w : lstword){
			if(p.containsKey(w)){
				p.put(w,p.get(w)+1);
				}
			else{
				p.put(w, 1);
			}
				
		}
		System.out.println(p);              
	}
	
	public String hasWord(String word){
		String[] lstword = word.split("\\s");
		Map<String, Integer> hm = new HashMap<String, Integer>();	
	     for (String w2 : lstword) {
	    	if(hm.containsKey(w2)){
				hm.put(word, hm.get(w2)+1);
				}
			else{
				hm.put(word, 0);
			}
				
		}
		System.out.println(hm);      
	    
		return null; 
		
	} 
	
	
}

